
public class Card{
	private String faceName;
	private int faceValue;
	private String suit; 
	public Card(String suit, String faceName, int faceValue) {
		this.suit = suit;
		this.faceName = faceName;
		this.faceValue = faceValue;
	 }

	 public String toString() {
	 	return faceName +" of "+ suit  +" and value is "+ faceValue;
	 }

	 public static void main(String[]args) {
	 	Card aceOfSpades = new Card("Spades", "Ace", 1);
	 	Card queenOfHearts = new Card("Hearts", "Queen", 12);
	 	System.out.println(aceOfSpades);
	 	System.out.println(queenOfHearts);
	 }
}