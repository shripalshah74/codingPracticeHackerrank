import java.util.*;
import java.security.SecureRandom;
class DeckOfCards{
	private Card[] deck;
	private int currentCard; //index of the next card to be delt

	/**
	* Constructor to build a deck of cards
	*/

	public DeckOfCards(){
		String [] faces = {"2","3","4","5","6","7","8","9","10","Jack","Queen","King", "Ace"};
		String [] suits = {"Diamonds", "Clubs","Hearts", "Spades"};
		deck = new Card[52];
		currentCard = 0;

		for(int suit = 0; suit<4;suit++)
		{
			for (int face = 0; face<13; face++) 
			{
				deck[face + suit*13] = new Card(suits[suit], faces[face],face+2 );
			}
		}  
	}// End of the constructor

	public void displayDeck() {
		for(Card card:deck)
			System.out.println(card);
	}


	// This method is to shuffle the card objects in the deck
	public void shuffle () 
	{
		currentCard = 0;
		SecureRandom  randomNumber = new SecureRandom();

		// for each caard in the deck, pick another random card and swap them

		for (int first = 0 ; first <deck.length; first++)
		{
			// select a Random card
			int second = randomNumber.nextInt(52);
			// swap the cards

			Card temp = deck[first];
			deck[first] = deck[second];
			deck[second] = temp;
		}
	}

	public static void main(String[]args) {
		DeckOfCards deck= new DeckOfCards();
		deck.displayDeck();
		deck.shuffle();
		deck.displayDeck();
	}
}