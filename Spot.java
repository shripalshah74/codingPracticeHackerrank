public class Spot {
	private int id;
	private int license;
	private int size;
	private boolean occupied= false;
	Car car;

	public Spot (int id, int size) {
		this.id= id;
		this.license = license;
		this.size = size;
	}

	public Car getCar(){
		return car;
	} 
	public boolean parkCar(Car car) {

		if(car.getSize()<=this.size){
			this.license = car.getLicense();
			occupied = true;
			this.car= car;
			return true;
		}
		else{
			System.out.println("Size mismatch");
			return false;
		}
	}

	public boolean removeCar() {
		license = 0;
		occupied = false;
		car = null;
		return true;
	}

	public int getId(){
		return id;
	}


	public boolean vacantSpot(){
		if (occupied==true)
		{
			this.license = 0;
			occupied = false;
			car = null;
			return true;

		}
		else{

			System.out.println("Spot Vacant!");
			return false;
		}
	}

	public String toString () {
		return "Car with license number: " +license+ " s"+ car+ " is parked on this spot " + id; 
	}

}