public class ParkingLot {
	private int size;
	private Spot[] spots;
	private int levels; 
	private static int i=0;


	public ParkingLot(int size, int level){
		this.size= size;
		levels=level;
		spots = new Spot[size];
		for(int le=0; le< levels; le++)
		for (int spot=(size/levels)*le; spot<(size/levels)*(le+1);spot ++) {
			spots[spot] =new Spot(spot, 1); 
		}

	}
	public boolean parkCar(int license, int sizeOfCar, String color ) {
		Car car= new Car(license, sizeOfCar, color, spots[i]);
		boolean result =  spots[i].parkCar(car);
		
		

		displaySpot();

		boolean result2 = spots[i].removeCar();
		i++;
		displaySpot();
		return result;
	}

	public boolean removeCar(){
		// Car car = spots[i].getCar();

		return true;
	}

	public void displaySpot(){
		for (int le =0; le<levels;  le++){
			System.out.println(" ");
			System.out.println(" ");
			System.out.println(" ");
			System.out.println(" Level: "+ le);
			for(int spot=(size/levels)*le; spot<(size/levels)*(le+1); spot++){
				System.out.println(spots[spot]);
			}

		}
		return;
	}	

	public static void main (String [] args) {
		ParkingLot lot = new ParkingLot(20, 4);
		lot.displaySpot();
		boolean park = lot.parkCar(7455, 1, "White");
		System.out.println(park);


	}

}