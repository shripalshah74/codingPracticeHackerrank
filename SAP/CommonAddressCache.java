public class CommonAddressCache {


	/*
		A method to create new address. Should be called when the address is not found in the cache or from the databast
	*/
	public Address createAddress(List<String> streets, String city, String state, String postalCode, String countryCode) {
		/*
			List of streets
			String of city
			String of state
			String of postalCode
			countryCode
					Return Type is Address class
		*/

		/*
			Create the address instance of Address class
		*/

			Address address = new Address();

			/*
				Set the information
			*/

			StringBuffer street = new StringBuffer();
			/*
				Getting the size of streets List
			*/

			int streetSize = streets.size();
			for(int i = 0; i<streetSize;i++) {
				/*
					form the street String by appending informtaion of streets List
				*/
				street.append(streets.get(i));

				/*
					Adding space after each append in the String street
				*/

				if(i!=streetSize-1) {
					street.append ("\n");
				}

			}
			address.setLines(street.toString());
			address.setCity(city);
			address.setState(state);
			address.setPostalCode(postalCode);
			address.setCountry(countryCode);
			return address;
		
		}

	/*
				Method for searching the address from the database.
				Return type is of Address class	
	*/

	/*
				You will pass the address to search as the parameters to this method
	*/
	public Address searchDB(String lines, String city, Strin state, String postalCode, String countryCode) {
		Address address = null;

		//Skipping the detail od DB search. If address is found in DB, set address and return. Otherwise, return null. 
		// The search address is slow, it approximately will take 5s.

		return address;


	}
/*
	this method is for getting the address from the db. 
	So in this, you firstly need to check if the address is in the cache which I willl be designing. If it is present in the cache,
	then return it from there. else query the searchDB method. If the addess returned is null, then create the address by calling 
	the create address method 

*/
	public Address getAddress(String lines, String city, String state, String postalCode, String countryCode) {
		Address address = null;
		// My code mechanism


		return address;
	}
public void parseXml(String fileLocation) {
	try {
			File fXmlFile = new File(locFile);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);

			//optional, but recommended
			//read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
			doc.getDocumentElement().normalize();

			System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

			NodeList nList = doc.getElementsByTagName("PostalAddress");
			System.out.println("_________________________________________");
			for(int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				System.out.println("\nCurrent Element :" + nNode.getNodeName());
				if(nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					NodeList nStreet = eElement.getElementsByTagName("Street");
					Element line = null;
					List<String> streetsList = new ArrayList<String>();
						if (nStreet != null) {

							System.out.println("Strets not null" + nStreet);
					String streets = eElement.getElementsByTagName("Street").item(0).getTextContent();
					//List<String> streetsList = Arrays.asList(streets.split(" "));
					
					List<String> streetTemp= new ArrayList<String>();
					for (int j= 0; j < nStreet.getLength(); j++) {
							line = (Element)nStreet.item(j);
							if(line != null) {
								streets = line.getTextContent();
								if(streets != ""){
								streetTemp = Arrays.asList(streets.split(" "));
								streetsList.addAll(streetTemp);
								System.out.println("Test: "+ line.getTextContent());
							}
								//streetsList.add(getCharacterDataFromElement(line));
							}
							
						}

					}
						
					System.out.println(" Street  combined: "+ streetsList );
					String city = eElement.getElementsByTagName("City").item(0).getTextContent();
					System.out.println("City : "+ city);
					String state = eElement.getElementsByTagName("State").item(0).getTextContent();
					System.out.println("State : " + state);
					String postalCode = eElement.getElementsByTagName("PostalCode").item(0).getTextContent();
					System.out.println("Postal Code : " + postalCode);
					String country = eElement.getElementsByTagName("Country").item(0).getTextContent();
					System.out.println("Country Code : " + country);


				}
			}
    }
    catch (Exception e) {
	e.printStackTrace();
    }
	}




public static void main (String [] args) {
		 String locFile = "D:/hackerrank/SAP/invoice.xml";
		 parseXml(locFile);
	}

}


/*
	The class Address

*/

public class Address {

	public String city;
	public String state;
	public String postalCode;
	public String countryCode;
	public String lines;

	public Address() {	}

	public Address(String city, String state, String postalCode, String countryCode, String lines){
		this.city = city;
		this.state = state;
		this.postalCode = postalCode;
		this.countryCode = countryCode;
		this.lines = lines;
	}

}
