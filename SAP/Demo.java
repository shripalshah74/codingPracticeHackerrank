import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
import java.util.*;
public class Demo{

	public static void main (String [] args) {
		try {
			File fXmlFile = new File("D:/hackerrank/SAP/invoice.xml");
	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();

			System.out.println("Root element: " + doc.getDocumentElement().getNodeName());
			NodeList nList = doc.getElementsByTagName("PostalAddress");
			System.out.println("_________________________________________");
			for(int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				System.out.println("\nCurrent Element :" + nNode.getNodeName());
				if(nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					String streets = eElement.getAttribute("Street");
					List<String> streetsList = Arrays.asList(streets.split(" "));	
					System.out.println(" Street : "+ streetsList );
					String city = eElement.getAttribute("city");
					System.out.println("City : "+ city);
					String state = eElement.getAttribute("State");
					System.out.println("State : " + state);
					String postalCode = eElement.getAttribute("PostalCode");
					System.out.println("Postal Code : " + postalCode);
					String country = eElement.getAttribute("Country");
					System.out.println("Country Code : " + country);


				}
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}

}