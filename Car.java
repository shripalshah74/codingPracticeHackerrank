public class Car  {
	private int license;
	private int size;
	private String color;
	Spot spot;

	public Car(int license, int size, String color, Spot spot) {
		this.spot = spot;
		this.license = license;
		this.size = size;
		this.color = color;
	}

	public void setLicense(int license) {
		this.license = license;
	}

	public int getLicense(){
		return license;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public int getSize() {
		return size;
	}


	public String toString () {
		return "The information about the car is " +" " +" License Number: " + license +" Size: "+ size +" Color: "+ color; 
	}
}